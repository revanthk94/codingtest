'use strict'

var module = angular.module('demo.controllers', []);
module.controller("CustomerController", [ "$scope", "CustomerService",
		function($scope, CustomerService) {
	
			$scope.customerSearchDto = {
				fromDate : '',
				toDate : ''
			};
			
//			function formatDate(d)
//			 {
//			  date = new Date(d)
//			  var dd = date.getDate(); 
//			  var mm = date.getMonth()+1;
//			  var yyyy = date.getFullYear(); 
//			  if(dd<10){dd='0'+dd} 
//			  if(mm<10){mm='0'+mm};
//			  return d = yyyy+'-'+mm+'-'+dd
//			};
//			
//			if(fromDate){
//			fromDate = formatDate(fromDate);
//			toDate = formatDate(toDate);
//			}
			
			
			$scope.searchCustomerJobs = function() {
				
					CustomerService.searchCustomerJobs($scope.customerSearchDto).then(function(value) {
						$scope.allCustomerJobs = value.data;
					}, function(reason) {
						console.log("error occured");
					}, function(value) {
						console.log("no callback");
					});

					$scope.customerSearchDto = {
						customerName : null,
						fromDate : '',
						toDate : ''
					};
				}
		} ]);