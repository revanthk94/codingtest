package com.codeassignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.codeassignment.model.Customer;
import com.codeassignment.model.Job;
import com.codeassignment.repo.CustomerRepository;

@SpringBootApplication
public class CustomerPortalApplication {

	@Autowired
	CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(CustomerPortalApplication.class, args);
	}

	@PostConstruct
	public void setupDbWithData() {
		Customer customer = new Customer();
		customer.setCustomerName("Revanth");
		customer.setId(1);
		customer.setState("MI");
		customer.setContactNumber("6605009116");

		List<Job> jobs = new ArrayList<>();
		Job job = new Job();
		job.setCustomer(customer);
		job.setId(21);
		job.setJobStatus("Pending");
		job.setJobType("Cable");
		job.setMissComment("No");
		job.setNotificationSent("Yes");
		job.setReturnMessage("No");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		try {
			job.setDueDate(sdf.parse("2018-05-16"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jobs.add(job);

		job = new Job();
		job.setCustomer(customer);
		job.setId(22);
		job.setJobStatus("Completed");
		job.setJobType("Internet");
		job.setMissComment("Yes");
		job.setNotificationSent("No");
		job.setReturnMessage("Yes");
		try {
			job.setDueDate(sdf.parse("2018-05-05"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		jobs.add(job);

		customer.setJobs(jobs);

		customer = customerRepository.save(customer);

		// User user = new User("Ashish", null);
		// user.setSkills(Arrays.asList(new Skill("java"), new Skill("js")));
		//
		// user = userRepository.save(user);

	}
}
